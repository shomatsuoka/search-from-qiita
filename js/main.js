var app = new Vue({
    el: '#app',
    data: {
        items: null,
        keyword: '',
        message: '',
        errorAlert: ''
    },
    watch: {
        /**
         * 検索用テキストボックスの状態をChangeで監視をする関数です
         * @param  {String} newKeyword 新しく入力された文字です
         * @param  {String} oldKeyword 新しく入力される前の文字です
         */
        keyword: function(newKeyword, oldKeyword) {
            this.message = 'Waiting for you to stop typing...'
            this.debouncedGetAnswer();
        }
    },
    created: function() {
        // DOMにアクセスする必要がなければ、mountedよりcreatedが早いとされます。
        this.keyword = 'JavaScript'
    },
    mounted: function() {
        /**
         * _.debounce(this.ファンクション名, 待ち時間)は、最後に実行された1000ミリ秒後に「this.ファンクション名」が実行されるlodashの関数です。
         */
        this.debouncedGetAnswer = _.debounce(this.getAnswer, 1000);
        this.countDates();
    },
    methods: {
        mouseEnterUserId:       function(e){},
        mouseLeaveFromUserId:   function(e){},
        mouseEnterUserImg:      function(e){},
        mouseLeaveFromUserImg:  function(e){},
        mouseEnterCard:         function(e){},
        mouseLeaveFromCard:     function(e){},
        getAnswer: function() {
            this.message = 'Loading...'
            this.loadData();
        },
        loadData: function() {
            var vm = this
            var params = {
                page: 1,
                per_page: 20,
                query: this.keyword
            }

            // axios.get('https://error-check/', { params })
            axios.get('https://qiita.com/api/v2/items', { params })
                .then(function(response) {
                    console.log(response)
                    vm.items = response.data;

                    if (vm.items.length == 0 && vm.keyword != '') {
                        vm.errorAlert = '<p class="error-alert"><em>「' + vm.keyword + '」</em>に一致する記事は見つかりませんでした。</p>'
                    } else if (vm.items.length == 0 && vm.keyword == ''){
                        vm.errorAlert = '<p class="error-alert"><em>検索をして下さい</em></p>'
                    } else {
                        vm.errorAlert = '';
                    }

                })
                .catch(function(error) {
                    vm.message = 'Error!' + error;
                    var error = String(error);

                    vm.errorAlert = '<p class="error-alert"><em>' + error + '</em></p>'
                    if (error == 'Error: Request failed with status code 403') {
                        vm.errorAlert =
                            '<p class="error-alert">' +
                            '<em class="status-code">' + error + '</em>' +
                            '<span>' +
                            '時間を空けてまたアクセスして下さい。' +
                            '（<a href="https://qiita.com/api/v2/docs#利用制限">Qiita API v2 利用制限</a>）' +
                            '</span>' +
                            '</p>'
                    }

                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        console.log('error.response.data:')
                        console.log(error.response.data);
                        console.log('----------');
                        console.log('error.response.status:'); // 例：400
                        console.log(error.response.status); // 例：400
                        console.log('----------');
                        console.log('error.response.statusText:'); // Bad Request
                        console.log(error.response.statusText); // Bad Request
                        console.log('----------');
                        console.log('error.response.headers:');
                        console.log(error.response.headers);
                        console.log('----------');
                    } else if (error.request) {
                        // The request was made but no response was received
                        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                        // http.ClientRequest in node.js
                        console.log('error.request:');
                        console.log(error.request);
                        console.log('----------');
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        console.log('Error:');
                        console.log(error.message);
                        console.log('----------');
                    }
                    console.log('error.config:');
                    console.log(error.config);
                    console.log('----------');
                    console.log('error:');
                    console.log(error)
                    console.log('----------');
                })
                .finally(function() {
                    vm.message = ''
                    vm.countDates();
                })
        },
        countDates: function() {
            if (this.items != null) {
                var date = new Date();
                var currentYear = date.getFullYear();
                var currentMonth = date.getMonth() + 1;
                var currentDate = date.getDate();
                var currentHour = date.getHours();
                var currentMin = date.getMinutes();
                var currentSec = date.getSeconds();

                for (var i = 0; i < this.items.length; i++) {
                    const postYear = Number(this.items[i].updated_at.slice(0, 4));
                    const postMonth = Number(this.items[i].updated_at.slice(5, 7));
                    const postDate = Number(this.items[i].updated_at.slice(8, 10));
                    const postHour = Number(this.items[i].updated_at.slice(11, 13));
                    const postMin = Number(this.items[i].updated_at.slice(14, 16));
                    // const postSec = Number(this.items[i].updated_at.slice(17, 19));

                    if (postYear == currentYear) {
                        // this.items[i].updated_at = 'This year'
                        if (postMonth == currentMonth) {
                            // this.items[i].updated_at = 'This month'
                            if (postDate == currentDate) {
                                // this.items[i].updated_at = 'Today';
                                if (postHour == currentHour) {
                                    if (postMin == currentMin) {
                                        this.items[i].updated_at = 'Just Now';
                                    } else {
                                        if (currentMin - postMin == 1) {
                                            this.items[i].updated_at = 'Just Now'
                                        } else if (currentMin > postMin) {
                                            this.items[i].updated_at = currentMin - postMin + ' minutes ago'
                                        }
                                    }
                                } else {
                                    if (currentHour - postHour == 1) {
                                        this.items[i].updated_at = '1 hour ago'
                                    } else if (currentHour > postHour) {
                                        this.items[i].updated_at = currentHour - postHour + ' hours ago'
                                    }
                                }
                            } else {
                                if (currentDate - postDate == 1) {
                                    this.items[i].updated_at = '1 day ago'
                                } else
                                if (currentDate > postDate) {
                                    this.items[i].updated_at = currentDate - postDate + ' days ago'
                                }
                            }
                        } else {
                            if (currentMonth - postMonth == 1) {
                                this.items[i].updated_at = 'Last month'
                            } else if (currentMonth > postMonth) {
                                this.items[i].updated_at = currentMonth - postMonth + ' months ago'
                            }
                        }
                    } else {
                        if (currentYear - postYear == 1) {
                            this.items[i].updated_at = 'Last year'
                        } else if (currentYear > postYear) {
                            this.items[i].updated_at = currentYear - postYear + ' years ago'
                        }
                    }
                }
            }
        }

    }
})